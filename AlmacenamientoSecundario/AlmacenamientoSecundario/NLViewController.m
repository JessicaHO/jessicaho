//
//  NLViewController.m
//  AlmacenamientoSecundario
//
//  Created by Patlan on 28/03/14.
//  Copyright (c) 2014 itlm. All rights reserved.
//

#import "NLViewController.h"

@interface NLViewController ()
@property (weak, nonatomic) IBOutlet UITextField *texto;

@end

@implementation NLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.texto.text=[self leerDatos];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSString *)leerDatos{
    NSString *directorio=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) objectAtIndex:0];
    NSString *rutaCompleta=[directorio stringByAppendingPathComponent:@"archivo"];
    
    NSStringEncoding codificacion = NSUTF8StringEncoding;
    NSError *error;
    
    NSString *textoLeido= [NSString stringWithContentsOfFile:rutaCompleta encoding:codificacion error:&error];
    if (textoLeido==nil) textoLeido=@"no inicializado";
    return textoLeido;
}

@end
