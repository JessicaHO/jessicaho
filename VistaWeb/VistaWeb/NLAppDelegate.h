//
//  NLAppDelegate.h
//  VistaWeb
//
//  Created by Patlan on 02/04/14.
//  Copyright (c) 2014 organizacion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
