//
//  NLAppDelegate.h
//  ServicioJSON
//
//  Created by Patlan on 10/04/14.
//  Copyright (c) 2014 organizacion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
