//
//  NLViewController.m
//  NotificacionesLocales
//
//  Created by Patlan on 28/03/14.
//  Copyright (c) 2014 itlm. All rights reserved.
//

#import "NLViewController.h"

@interface NLViewController ()
- (IBAction)notificar:(id)sender;

@end

@implementation NLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)notificar:(id)sender {
    NSDate *alertTime = [[NSDate date]dateByAddingTimeInterval:10];
    UIApplication* app = [UIApplication sharedApplication];
    UILocalNotification* notifyAlarm = [[UILocalNotification alloc]init];
    
    if (notifyAlarm)
    {
        notifyAlarm.fireDate = alertTime;
        notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
        notifyAlarm.repeatInterval = 0;
        [notifyAlarm setApplicationIconBadgeNumber:1];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 1;
        notifyAlarm.soundName = @"Glass.aiff";
        notifyAlarm.alertBody =@"Esta es una notificacion";
       
        [app scheduleLocalNotification:notifyAlarm];
        
    }
}
@end
