//
//  MOAppDelegate.h
//  EjemploModal
//
//  Created by Patlan on 26/03/14.
//  Copyright (c) 2014 itlm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
