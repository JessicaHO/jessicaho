//
//  main.m
//  EjemploModal
//
//  Created by Patlan on 26/03/14.
//  Copyright (c) 2014 itlm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MOAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MOAppDelegate class]));
    }
}
