//
//  NLViewController.m
//  VistaWeb
//
//  Created by Patlan on 02/04/14.
//  Copyright (c) 2014 organizacion. All rights reserved.
//

#import "NLViewController.h"

@interface NLViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *navegador;

@end

@implementation NLViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	NSString *direccion = @"http://www.criptidosdigitales.com";
    //si quieres ir a una pagina menos conocida sustituye criptidosdigitales por google
    
    NSURL *url = [NSURL URLWithString:direccion];
    NSURLRequest *peticion = [NSURLRequest requestWithURL:url];
    
    [self.navegador loadRequest:peticion];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
