//
//  main.m
//  AlmacenamientoSecundario
//
//  Created by Patlan on 28/03/14.
//  Copyright (c) 2014 itlm. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([NLAppDelegate class]));
    }
}
